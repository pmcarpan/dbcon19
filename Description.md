**Authors:** Arpan Basu, Riktim Mondal, Manosij Ghosh, Vivek Roy, Showmik Bhowmik, Ram Sarkar

**Method Name:** JU_CMATER_1

We use a U-NET based convolutional network with skip-connections between the down-sampling and up-sampling layers. 
The image is first down-sampled using convolutional blocks with a stride of 2, and then up-sampled using transposed convolutional 
blocks to the original dimension. The input to this model consists of 256 x 256 patches of the original images. This means an 
input is a 256 x 256 x 3 tensor. The output produced by the model is a 256 x 256 x 1 tensor with each value between 0 and 1. This 
is concatenated along the last dimension to produce a 256 x 256 x 3 greyscale patch. The model is trained using the Adam optimizing 
algorithm using binary cross-entropy between each input patch and the corresponding ground-truth patch as the loss. The image is then 
thresholded at 0.5 to produce a binarized patch. To produce output, we take 256 x 256 overlapping patches across the image, suitably 
padded with edge pixels if required, with a stride of 64 along both horizontal and vertical directions.