## Notes

Our main system is the Windows executable. It is recommended to try with either of `unet_main_cpu.exe` or `unet_main_gpu.exe`.

We also recommend to perform a sample run on `6.bmp` present in the `Temp` directory and verify that the output matches 
with `6_out.bmp` present in the same directory.

We have tested with bmp and png image formats thoroughly, and recommend the same.
Jpg can be used, but as it is a lossy format, it is not recommended.

Sometimes, permission to execute may be required on ubuntu. In this case `chmod +x` should be used.

## Contents

1. **unet\_main\_cpu.exe** - tensorflow-cpu based executable for windows
2. **unet\_main\_cpu** - tensorflow-cpu based executable for ubuntu
3. **unet\_main\_gpu.exe** - tensorflow-gpu based executable for windows
4. **unet\_main\_gpu** - tensorflow-gpu based executable for ubuntu
5. **TrainCheckpointsNew** - directory containing the saved tensorflow model information
6. **Description.md** - contains a brief write-up on the approach
7. **Temp** - directory containing sample input and output images

## Steps for downloading

The zipped archive can be downloaded by clicking on the **download** button in the top-right.
(Below the blue **clone** button.)

Individual files can be downloaded by clicking on them and then clicking on the **download** button.

The repository can also be cloned using git by the command

    git clone https://gitlab.com/pmcarpan/dbcon19.git

## Instructions for running on windows

1. Open a command prompt window in the extracted/cloned directory.
2. **The TrainCheckpointsNew directory must be in the same directory as the executable being run.**
3. Execute the desired command from command prompt for running on cpu or gpu. 

Command for running on tf-cpu is

    unet_main_cpu.exe path/to/input/input_file_name.bmp path/to/output/output_file_name.bmp

Command for running on tf-gpu is

    unet_main_gpu.exe path/to/input/input_file_name.bmp path/to/output/output_file_name.bmp
    
For example, running the following command

    unet_main_cpu.exe Temp/6.bmp Temp/6_o.bmp
    
produces the output in `Temp/6_o.bmp` after running on input image `Temp/6.bmp`.

## Instructions for running on ubuntu

1. Open a terminal window in the extracted/cloned directory.
2. **The TrainCheckpointsNew directory must be in the same directory as the executable being run.**
3. Execute the desired command from terminal for running on cpu or gpu. 

Command for running on tf-cpu is

    ./unet_main_cpu path/to/input/input_file_name.bmp path/to/output/output_file_name.bmp

Command for running on tf-gpu is

    ./unet_main_gpu path/to/input/input_file_name.bmp path/to/output/output_file_name.bmp
    
For example, running the following command

    ./unet_main_cpu Temp/6.bmp Temp/6_o.bmp
    
produces the output in `Temp/6_o.bmp` after running on input image `Temp/6.bmp`.
